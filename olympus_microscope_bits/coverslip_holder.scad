use <../utilities.scad>;

slide = [25.6, 75.75, 1];
coverslip = [24+0.8,32+0.8,0.1];
corner_r = 2;
lip_r = 3;
lip_t=0.5;
$fn = 16;
d=0.05;

difference(){
	union(){
		hull(){
			for(x = [-1,1]*(slide[0]/2-corner_r)) for(y = [-1,1]*(slide[1]/2-corner_r)){
				translate([x,y,0]) cylinder(r1=corner_r+0.3, r2=corner_r, h=slide[2]);
			}
			translate([0,0,2]) cube([slide[0]-6,slide[1]-6,4],center=true);
		}
		assign(h=6) hull(){
			translate([0,0,h/2]) cube([slide[0]-6,slide[1]-12,h],center=true);
			translate([0,0,h/2]) cube([coverslip[0]+6,coverslip[1]+6,h],center=true);
		}
	}
	//coverslip
	hull(){
		translate([0,0,lip_t+coverslip[2]/2]) cube(coverslip,center=true);
		translate([7,7,lip_t+coverslip[2]/2+20]) cube(coverslip,center=true);
	}
	for(i=[-1,1]) for(j=[-1,1]) translate([i*coverslip[0]/2,j*coverslip[1]/2,lip_t]) cylinder(r=0.75,h=0.5);
	//cube([coverslip[0]-2*lip_r,coverslip[1]-2*lip_r,9999],center=true); //access from beneath
	difference(){
		cube([coverslip[0],coverslip[1],lip_t*2+d],center=true);
		for(i=[-1,1]) for(j=[-1,1]) translate([i*coverslip[0]/2,j*coverslip[1]/2,0]) rotate([0,0,45]) cube([1,1,9999]*lip_r*2,center=true);
	}

	//hole for clip
	translate([coverslip[0],coverslip[1],0]/2) rotate([0,0,-45]) translate([-lip_r,-lip_r*0.5,-d]) cube([999,5,2]);
	
	//general purpose mounting holes
	for(x = [-1,1]*12.5/2) for(y = [-1,1]*(slide[1]/2-10)) translate([x,y,0]) cylinder(r=1.4,h=999,center=true);
	//access to the coverslip from the sides
	hull(){
		translate([0,0,lip_t+9999/2]) cube([coverslip[0]-6,coverslip[1],9999],center=true);
		translate([0,0,lip_t+9999/2]) cube([coverslip[0]-12,coverslip[1]+8,9999],center=true);
		translate([0,0,4.5+9999/2]) cube([6,slide[1]-20,9999],center=true);
	}
}

/*
TODO
Version 1 worked but could do with more clearance.  Particularly, taking out the corners with cylindrical voids would help a lot.
A sprung clip and sides that come in as we go up would hold it nicely - no need for screw-based clamps then.
Think about printing a clip for the fibres
*/