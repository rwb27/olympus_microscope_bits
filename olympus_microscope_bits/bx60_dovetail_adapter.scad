//replacement dovetail for rotatable polariser
//this adapter converts from the large clamp on the BX60 to
//the smaller dovetail used by the BX51.  As defined by this file, it fits beautifully.
//it was printed with 0.24mm layers, 0.4 fill density, with support material (linear infill, 2.5mm spacing, no interface layers)

$fn=48;
d=0.05;
male_r=58.95/2+0.2/2; //second term is a fudge factor...
male_h=11.5;

outer_r=76/2; //top dovetail
inner_r=56/2;
height=21;

male_to_dt_h=4; //amount of material between top of male part and face of dovetail

dt_z=15; //height at which the dovetail bit starts
dt_h=5;  //dovetail height
//r1 is the radius of the widest part of the flange
//r2 is the radius of the narrowest part (i.e. the "neck" of the male dovetail)
dt_r2=40/2+0.7; //previously inner_r-7
dt_r1=46/2+0.7; //previously inner_r-5
dt_angle=130; //angle around which we clamp the dovetail

difference(){
	union(){
		translate([0,0,dt_z-male_to_dt_h-male_h]){
			translate([0,0,0])		cylinder(h=1.5+d,r1=male_r-1,r2=male_r);
			translate([0,0,1.5])	cylinder(h=1.5+d,r=male_r);
			translate([0,0,3])		cylinder(h=1.5+d,r1=male_r,r2=male_r-1);
			translate([0,0,4.5])	cylinder(h=1.5+d,r1=male_r-1,r2=male_r);
			translate([0,0,6])		cylinder(h=5.5+d,r=male_r);
		}
		translate([0,0,dt_z-male_to_dt_h]) cylinder(r=outer_r,h=height-dt_z+male_to_dt_h); //top part
	
	}

	cylinder(r=36/2,h=999,center=true);	//clearance for beam (same as inner dovetail)
	translate([0,0,dt_z-1]) cylinder(r=dt_r2,h=999);	//clearance for beam (same as inner dovetail)
	translate([0,0,dt_z]) cylinder(r1=dt_r1, r2=dt_r2, h=dt_h); //dovetail

	translate([0,0,dt_z]) difference(){
		cylinder(r1=inner_r-3,r2=inner_r,h=(height+0.1-dt_z));
		linear_extrude(999,center=true)		polygon([[0,0],[sin(dt_angle/2),cos(dt_angle/2)],[-sin(dt_angle/2),cos(dt_angle/2)]]*999);
	}

	translate([0,0,dt_z+dt_h/2+1]) rotate([90,0,0]) cylinder(r=1.9,h=999);	

}