use <../utilities.scad>;
use <bx51_dovetail.scad>;
use <../standard_optomechanics/cylinder_clamp.scad>;

obj_clamp_h=15;
obj_clamp_z=25;
top_z=obj_clamp_z+obj_clamp_h/2;
obj_travel=3;
obj_bottom_z = top_z - obj_clamp_h - obj_travel;
sm1_radius=15.25+0.2;
obj_lever_l=12;
flex_l=0.7;
flex_t=0.73;
obj_clamp_flat_y=-(sm1_radius+4);
actuator_z=7.5;
actuator_y=obj_clamp_flat_y-flex_l-4;
actuator_angle=3;
flexure_anchor_y=obj_clamp_flat_y-obj_lever_l;
bar_r=12.5/2+0.3;

$fn=48;
d=0.05;

module objective_clamp(){ //clamp for objective and flexures
	union(){
		mirror([0,0,1]) cylinder_clamp(inner_r=sm1_radius, clamp_h=obj_clamp_h, mounting_bolt=0, flat_width=20, flat_t=4);
		repeat([0,0,-obj_clamp_h+3], 2) mirror([0,1,0]) translate([0,-obj_clamp_flat_y,obj_clamp_h/2]) reflect([1,0,0]){
			translate([-10,-d,-flex_t]) cube([4, obj_lever_l + 2*d, flex_t]);
			translate([-10,flex_l,-3]) cube([4, obj_lever_l - 2*flex_l, 3]);
		}
	}
}

module objective_actuating_lever(){
	translate([0,actuator_y+4,0]) mirror([0,1,0]) difference(){
		sequential_hull(){ //flexure actuating lever
			translate([-10,0,top_z-3]) cube([20,obj_lever_l-2*flex_l,3]); //join top flexures together
			translate([-4,0,top_z-5]) cube([8,obj_lever_l-2*flex_l,d]); //top of lever
			translate([-4,1.5,top_z-obj_clamp_h-1]) cube([9,5,d]); //middle of lever (clearance for clamp)
			translate([-4,0,actuator_z+3]) cube([8,4,d]); //top of actuator flat
			translate([-4,0,actuator_z-3]) cube([8,4,d]); //bottom of actuator flat
		}
		translate([0,0,actuator_z]) cube([999,2,2],center=true);
	}
}

module objective_clamp_silhouette(){
	projection() hull() objective_clamp();
}
module clamp_clearance(h=d){
	linear_extrude(h){
		minkowski(){
			objective_clamp_silhouette();
			circle(r=1,$fn=12);
		}
	}
}
module outer_shell(h=1){
	linear_extrude(h){
		minkowski(){
			objective_clamp_silhouette();
			circle(r=3,$fn=12);
		}
	}
}
module actuator(){
	union(){
		translate([0,0,2]) sphere(r=2,$fn=8);
		translate([0,0,2]) cylinder(r=3,h=10,$fn=16);
		translate([0,0,8+4]) cylinder(r=9.5/2,h=22-8,$fn=16);
		translate([0,0,26]) cylinder(r=7,h=30,$fn=16);
		translate([0,0,56]) cylinder(r=9,h=30,$fn=16);
	}
}
module actuator_frame(){
	translate([0,actuator_y, actuator_z]) rotate([90-actuator_angle,0,0]){
		children();
	}
}

mirror([0,0,1]) 
difference(){
	union(){
		mirror([0,0,1]) bx51_dovetail_female(height=10, outer_r=33, beam_r=14);

		translate([0,0,obj_clamp_z]) objective_clamp(); //includes flexures
		objective_actuating_lever(); //lever to move flexures
		translate([-10,obj_clamp_flat_y-obj_lever_l-2,top_z-obj_clamp_h]) cube([20,2,obj_clamp_h]);

		difference(){
			union(){
				sequential_hull(){ //outside of block
					union(){
						translate([0,0,top_z-d]) outer_shell(d);
						translate([0,flexure_anchor_y-10,top_z-d]) cube([10,d,2*d],center=true);
					}
					union(){
						translate([0,0,obj_bottom_z]) outer_shell(d);
						actuator_frame() translate([0,0,26-10/2]) cube([16,2*(actuator_z+16*sin(actuator_angle)),10],center=true); //flat for micrometer
					}
					cylinder(r=33,h=0.5);
				}
				//clamps for bracing bars
				reflect([1,0,0]) translate([0,flexure_anchor_y-12/2,top_z]) mirror([0,0,1]){
					translate([75/2,0,bar_r+4]) mirror([1,0,0]) cylinder_clamp_v(bar_r,12,mounting_bolt=0,flat_t=4,flat_w=bar_r+d);
					hull(){
						translate([75/2-bar_r-4,-6,0]) cube([4,12,bar_r+4]);
						translate([0,-4,0]) cube([4,8,bar_r+4]);
					}
				}
				reflect([1,0,0]) translate([0,8,top_z]) mirror([0,0,1]){
					translate([75/2,0,bar_r+4]) mirror([1,0,0]) cylinder_clamp_v(bar_r,12,mounting_bolt=0,flat_t=4,flat_w=bar_r+d);
					hull(){
						translate([75/2-bar_r-4,-6,0]) cube([4,12,bar_r+4]);
						translate([0,-4,0]) cube([4,8,bar_r+4]);
					}
				}
			}
			translate([0,sm1_radius+5,obj_clamp_z]) cube([999,10,12],center=true); //access to clamping bolt
			sequential_hull(){																				//space inside (for clamp, flexures, actuator)
				translate([0,0,top_z]) clamp_clearance(h=10);
				translate([0,0,obj_bottom_z]) clamp_clearance();
				translate([0,0,actuator_z-3-1]) union(){
					cylinder(r=14,h=d);
					translate([-5,actuator_y-4,0]) cube([10,d,d]);
				}
				translate([0,0,actuator_z-3-2]) union(){
					cylinder(r=14,h=d);
					translate([-10,-14,0]) cube([20,14,d]);
				}
				translate([0,0,0]) cylinder(r=14,h=d);
				translate([0,0,-999]) cylinder(r=14,h=d);
			}
//			translate([0,actuator_y-4-22+10,actuator_z]) rotate([-90,0,0]) cylinder(r=8,h=4); //just to make sure we've got clearance for the actuator
		}
		//micrometer barrel is 9.5mm, clamp needs to be >9.5mm long, shoulder-to-tip is >=22mm, allow for 4mm travel?
	}
	actuator_frame() translate([-7.5,-999+7.5+1,26-10-5]) cube([15,999,5]); //micrometer lockring
	actuator_frame() translate([0,0,2]) rotate([90,0,0]) cylinder_with_45deg_top(r=9.5/2+0.3,h=999,$fn=24); //micrometer barrel (and clearance for pin)
	reflect([1,0,0]) translate([75/2,0,top_z-10]) rotate([90,0,0]) cylinder(r=12.5/2,h=200,center=true); //bars	
	reflect([1,0,0]) translate([0,0,actuator_z-2]) rotate([0,0,167]) cube([2,9999,4]); //elastic band access
}

//% actuator_frame() actuator();