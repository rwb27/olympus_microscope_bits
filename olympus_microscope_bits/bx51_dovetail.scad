

//second round of measurements:
//dovetail large radius=46mm
//small radius ~ 40mm
//height=6mm

module bx51_dovetail_female(outer_r=33, height=10){
	assign(
		dt_z=height-6,
		dt_h=5,
		dt_r2=40/2+0.7, //previously inner_r-7
		dt_r1=46/2+0.7, //previously inner_r-5
		dt_angle=130,
		inner_r=56/2 //this sets the radius of the non-dovetail part, best leave it or there won't be enough clearance!
	) difference(){
		cylinder(r=outer_r,h=height, $fn=48);
	
		cylinder(r=30/2,h=999,center=true, $fn=48);	//clearance for beam (same as inner dovetail)
		translate([0,0,dt_z-1]) cylinder(r=dt_r2,h=999, $fn=48);	//clearance for beam (same as inner dovetail)
		translate([0,0,dt_z]) cylinder(r1=dt_r1, r2=dt_r2, h=dt_h, $fn=48); //dovetail
	
		translate([0,0,dt_z]) difference(){
			cylinder(r1=inner_r-3,r2=inner_r,h=(height+0.1-dt_z), $fn=48);
			linear_extrude(999,center=true)		polygon([[0,0],[sin(dt_angle/2),cos(dt_angle/2)],[-sin(dt_angle/2),cos(dt_angle/2)]]*999);
		}
	
		translate([0,0,dt_z+dt_h/2+0.5]) rotate([90,0,0]) cylinder(r=1.9,h=999, $fn=16);	
	}
}


module bx51_dovetail_male(outer_r=33, beam_r=14, base_h=0){
	assign(
		dt_h=5,
		dt_r2=40/2+0.25, //radius of top
		dt_r1=46/2+0.25, //radius of bottom
		d=0.05
	) difference(){
		union(){
			translate([0,0,base_h]) cylinder(r2=dt_r1, r1=dt_r2, h=dt_h, $fn=48); //dovetail
			if(base_h>0){
				translate([0,0,0]) cylinder(r=outer_r, h=base_h+d, $fn=48); //base
			}
		}
		cylinder(r=beam_r,h=999,center=true, $fn=48);	//clearance for beam
	}
}



//translate([0,0,20]) mirror([0,0,1]) bx51_dovetail_male(base_h=0);
//bx51_dovetail_female();

bx51_dovetail_male(base_h=2);