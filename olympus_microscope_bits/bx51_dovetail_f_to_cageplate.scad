$fn=48;

//second round of measurements:
//dovetail large radius=46mm
//small radius ~ 40mm
//height=6mm


outer_r=66/2;
inner_r=56/2;
height=21;

dt_z=15;
dt_h=5;
dt_r2=40/2+0.7; //previously inner_r-7
dt_r1=46/2+0.7; //previously inner_r-5
dt_angle=130;

//boltradius=(inner_r + outer_r)/2;

difference(){
	cylinder(r=outer_r,h=21);

	cylinder(r=30/2,h=999,center=true);	//clearance for beam (same as inner dovetail)
	translate([0,0,dt_z-1]) cylinder(r=dt_r2,h=999);	//clearance for beam (same as inner dovetail)
	translate([0,0,dt_z]) cylinder(r1=dt_r1, r2=dt_r2, h=dt_h); //dovetail

	translate([0,0,dt_z]) difference(){
		cylinder(r1=inner_r-3,r2=inner_r,h=(height+0.1-dt_z));
		linear_extrude(999,center=true)		polygon([[0,0],[sin(dt_angle/2),cos(dt_angle/2)],[-sin(dt_angle/2),cos(dt_angle/2)]]*999);
	}

	translate([0,0,dt_z+dt_h/2+1]) rotate([90,0,0]) cylinder(r=1.9,h=999);	

//	for(i=[0,1,2]*120) rotate([0,0,i]) translate([0,boltradius,0]) 

	for(i=[-1,1]) for(j=[-1,1]) translate([i,j,0]*15) union(){
		cylinder(r=1.2,h=9999,center=true,$fn=12);
		translate([0,0,8]) cylinder(r=3.5,h=999,$fn=12);
	}

	//translate([0,0,-0.1]) cylinder(r=99999, h=13);
}