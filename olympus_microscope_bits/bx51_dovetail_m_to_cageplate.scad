use <../utilities.scad>;
use <./bx51_dovetail.scad>;

$fn=48;

//second round of measurements:
//dovetail large radius=46mm
//small radius ~ 40mm
//height=6mm


outer_r=66/2;
inner_r=56/2;
height=21;

dt_z=15;
dt_h=5;
dt_r2=40/2+0.7; //previously inner_r-7
dt_r1=46/2+0.7; //previously inner_r-5
dt_angle=130;

//boltradius=(inner_r + outer_r)/2;

difference(){
	bx51_dovetail_male(base_h=10);	

	for(i=[-1,1]) for(j=[-1,1]) translate([i,j,0]*15) union(){
		cylinder(r=1.2,h=9999,center=true,$fn=12);
		translate([0,0,2]) cylinder(r=3.5,h=999,$fn=12);
	}

	//translate([0,0,-0.1]) cylinder(r=99999, h=13);
}