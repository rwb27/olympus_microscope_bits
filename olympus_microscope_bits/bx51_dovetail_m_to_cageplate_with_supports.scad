use <../utilities.scad>;
use <./bx51_dovetail.scad>;
use <../standard_optomechanics/cylinder_clamp.scad>;

difference(){
	union(){
		intersection(){
			bx51_dovetail_male(base_h=10);	
			cube([999,75-16,999],center=true);
		}
		reflect([0,1,0]) translate([0,37.5,5]) cylinder_clamp(inner_r=12.7/2+0.1,clamp_h=10,mounting_bolt=0);
	}

	for(i=[-1,1]) for(j=[-1,1]) translate([i,j,0]*15) union(){
		cylinder(r=1.2,h=9999,center=true,$fn=12);
		translate([0,0,2]) cylinder(r=3.5,h=999,$fn=12);
	}

	//translate([0,0,-0.1]) cylinder(r=99999, h=13);
}