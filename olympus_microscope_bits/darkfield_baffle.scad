h=45;
fin_h=15;
sm1_r=25.4/2+0.5;

difference(){
	union(){
		cylinder(h=50,r=7);
		for(a=[0,120,240]) rotate([0,0,a]) translate([-0.5,0,0]) cube([1,sm1_r,fin_h]);
	}
	cylinder(h=9999,r=6,center=true);
}