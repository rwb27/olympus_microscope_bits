$fn=48;

outer_r=76/2;
inner_r=56/2;
height=21;

dt_z=14;
dt_h=5;
dt_r2=inner_r - 7;
dt_r1=inner_r - 5;
dt_angle=130;

boltradius=(inner_r + outer_r)/2;

difference(){
	cylinder(r=outer_r,h=21);

	cylinder(r=dt_r2,h=999,center=true);	//clearance for beam (same as inner dovetail)
	translate([0,0,dt_z]) cylinder(r1=dt_r1, r2=dt_r2, h=dt_h); //dovetail

	translate([0,0,dt_z]) linear_extrude(999) difference(){
		circle(r=inner_r);
		polygon([[0,0],[sin(dt_angle/2),cos(dt_angle/2)],[-sin(dt_angle/2),cos(dt_angle/2)]]*999);
	}

	translate([0,0,dt_z+dt_h/2]) rotate([90,0,0]) cylinder(r=2.25,h=999);	

	for(i=[0,1,2]*120) rotate([0,0,i]) translate([0,boltradius,0]) union(){
		cylinder(r=3.3/2,h=9999,center=true,$fn=12);
		translate([0,0,16]) cylinder(r=6.6/2,h=999,$fn=12);
	}
}