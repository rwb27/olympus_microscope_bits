use <../utilities.scad>;

slide = [25.6, 75.75, 1];
coverslip = [24+0.8,32+0.8,0.1];
corner_r = 2;
lip_r = 3;
lip_t=0.5;
$fn = 16;
d=0.05;

union(){
	difference(){
		union(){
			hull(){
				for(x = [-1,1]*(slide[0]/2-corner_r)) for(y = [-1,1]*(slide[1]/2-corner_r)){
					translate([x,y,0]) cylinder(r1=corner_r+0.3, r2=corner_r, h=slide[2]);
				}
				translate([0,0,2]) cube([slide[0]-6,slide[1]-6,4],center=true);
			}
			assign(h=6) hull(){
				translate([0,0,h/2]) cube([slide[0]-6,slide[1]-12,h],center=true);
				translate([0,0,h/2]) cube([coverslip[0]+6,coverslip[1]+6,h],center=true);
			}
		}
		//coverslip
		hull(){
			translate([0,0,lip_t+coverslip[2]/2]) cube(coverslip,center=true);
			translate([7,7,lip_t+coverslip[2]/2+20]) cube(coverslip,center=true);
		}
		for(i=[-1,1]) for(j=[-1,1]) translate([i*coverslip[0]/2,j*coverslip[1]/2,lip_t]) cylinder(r=0.75,h=0.5);
		//cube([coverslip[0]-2*lip_r,coverslip[1]-2*lip_r,9999],center=true); //access from beneath
		difference(){
			cube([coverslip[0],coverslip[1],lip_t*2+d],center=true);
			for(i=[-1,1]) for(j=[-1,1]) translate([i*coverslip[0]/2,j*coverslip[1]/2,0]) rotate([0,0,45]) cube([1,1,9999]*lip_r*2,center=true);
		}
	
		//hole for clip
		translate([coverslip[0],coverslip[1],0]/2) rotate([0,0,-45]) hull(){
			translate([-lip_r*0.8,-lip_r*0.5,-d]) cube([999,5,2]);
			translate([-lip_r*0.8,-lip_r*0.5-1,2-d]) cube([999,5,d]);
		}
		
		//general purpose mounting holes
		for(x = [-1,1]*12.5/2) for(y = [-1,1]*(slide[1]/2-10)) translate([x,y,0]) cylinder(r=1.4,h=999,center=true);
		//access to the coverslip from the sides
		hull(){
			translate([0,0,lip_t+9999/2]) cube([coverslip[0]-6,coverslip[1],9999],center=true);
			translate([0,0,lip_t+9999/2]) cube([coverslip[0]-12,coverslip[1]+8,9999],center=true);
			translate([0,0,4.5+9999/2]) cube([6,slide[1]-20,9999],center=true);
		}
	}

	//clamping arm
	translate([coverslip[0],coverslip[1],0]/2) rotate([0,0,-45]){
		hull(){
			translate([-lip_r*0.66,-lip_r*0.33,0]) cube([lip_r*3,lip_r*1,d]);
			translate([-lip_r*0.66,-lip_r*0.33-1.3/2,1.3]) cube([lip_r*3,lip_r*1+1.3/2,d]);
		}
	}
	assign(r=sqrt(pow(coverslip[0],2)+pow(coverslip[1],2))/2 + lip_r/2) intersection(){
		difference(){
			cylinder(r=r, h=3,$fn=48);
			cylinder(r=r-2, h=999,center=true,$fn=48);
		}
		rotate([0,0,-52]) cube([999,999,999]);
		rotate([0,0,-48]) cube([999,999,999]);
	}
}

/*
TODO
Version 1 worked but could do with more clearance.
Version 2 seems pretty good, but clamping the corner of the coverslip seems an invitation to crack something.  The fit is neat but not tight, until the clamp is engaged at which point it's pretty nice.  Could beef up the clamping arm, but I'm worried about smashing the slide.
*/