# olympus_microscope_bits

Some OpenSCAD files I designed while working at the Nanophotonics Centre in Cambridge, to interface with Olympus microscopes.

The designs, along with the OpenSCAD source files, are in ``olympus_microscope_bits``.  I think I have copied the relevant dependencies in as well.

Please consider all this content licensed under the CERN Open Hardware License, Strongly reciprocal (CERN-OHL-S v2).