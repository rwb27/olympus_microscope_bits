use <../utilities.scad>;
//use <bx51_dovetail.scad>;
use <../standard_optomechanics/cylinder_clamp.scad>;

clamp_h=15;
sm1_radius=15.25;
beam_h=50;
camera_h=25;
mounting_hole_x = 8.25;


$fn=48;
d=0.05;

difference(){
	union(){
		translate([0,0,clamp_h/2]) cylinder_clamp(inner_r=sm1_radius, clamp_h=clamp_h, clamp_t=3, mounting_bolt=0, flat_width=0, flat_t=3);
        difference(){
            hull(){
                translate([0,0,clamp_h-d]) cylinder(r=sm1_radius + 3,h=d);
                translate([0,0,camera_h]) cylinder(r=12.5,h=2);
                translate([0,camera_h-clamp_h+sm1_radius,camera_h]) cube([25,d,4],center=true);
            }
            
            //beam clearance
            sequential_hull(){
                translate([0,0,clamp_h-2*d]) cylinder(r=sm1_radius, h=d);
                translate([0,0,camera_h-1]) cube([8,8,d],center=true);
                translate([0,0,camera_h]) cube([8,8,10],center=true);
            }
            
            //mounting holes
            reflect([1,0,0]) translate([mounting_hole_x,0,camera_h-5]) cylinder(r=0.6,h=999,$fn=12); 
            
            //clearance for PCB
            translate([0,0,camera_h-1]){
                translate([0,29-13,5]) cube([21.5,58.5,8],center=true);
                reflect([0,1,0]) translate([-4.5,6,-0.5]) cube([9,999,8]);
                translate([0,22.5,5]) cube([20.5,28,15],center=true);
                translate([-3,41,5]) cube([15,6,15],center=true);
            }
            
            //slit for clamp
            translate([-1.5,0,0]) cube([3,999,camera_h-5]);
        }
        //clip for end of PCB
        reflect([1,0,0]) translate([-21.5/2-d,camera_h-clamp_h+sm1_radius -3,camera_h])hull(){
            translate([0,0,0.75]) cube([d,3,d]);
            translate([0,0,1.75]) cube([1.1,3,0.25]);
        }
    }
    
}
 
  
/*        
difference(){
	union(){
		translate([0,0,1]) cageplate();
		translate([0,33,1]) cube([25,28,10],center=true);
	}
	//viewport hole
	cylinder(r1=8,r2=1,h=12,center=true);

	//clearance for PCB
	translate([0,29-13,5]) cube([21.5,58.5,8],center=true);
	translate([0,0,5]) cube([12,26,12],center=true);
	translate([0,22.5,5]) cube([21,33,15],center=true);
	translate([-3,41,5]) cube([15,6,15],center=true);

	//mounting holes
	for(p=[[-8.25,0,0],[8.25,0,0],[-4.5,-9,0],[6.7,42,0]]) 
		translate(p+[0,0,-2]) cylinder(r=0.5,h=999);

	//screw holes for mounting back
	for(p=[-1,1]) translate([6*p,-16.5,0]) cylinder(r=1.5,h=9);
	translate([0,47,0]) cube([4,2,2],center=true);

	//cable clearance
	translate([0,36,4.5]) rotate([0,90,0]) cylinder(h=999, r=1.8);
	translate([0,36-1.8,4.5]) cube([999,3.6,999]);

}	
*/