use <cylinder_clamp.scad>;
use <utilities.scad>;

micrometer_r = 4.75;
screw_sep = 15.3;
stop_screw_sep = 5.4;
screw_n = 3;
screw_d = 3;
micrometer_pos = [micrometer_r + screw_d + 1.5, micrometer_r + 3 + 0.5, 4.5];


difference(){
    union(){
        translate(micrometer_pos) 
            cylinder_clamp(inner_r=micrometer_r, clamp_h=micrometer_pos[2]*2, clamp_t=3,
                           flat_t=3, flat_width=0, mounting_bolt=0);
        translate([-screw_d-0.5,0,0]) cube([screw_d*2+1, micrometer_pos[1] + 5, screw_d*2+1 + screw_sep * (screw_n-1)]);
    }
    
    translate([0,5,screw_d + 0.5]) repeat([0,0,screw_sep], screw_n){
        rotate([-90,0,0]) cylinder(d=screw_d*2, h=999, $fn=16);
        rotate([-90,0,0]) cylinder(d=screw_d, h=999, center=true, $fn=16);
    }
}


translate([-stop_screw_sep - screw_d - 3,0,0])
difference(){
    w=10;
    sequential_hull(){
        cylinder(r=w/2,h=2);
        translate([-w/2,10-3,0]) cube([w,3,micrometer_pos[1]+3]);
        translate([-w/2,10-3,0]) cube([w,3,micrometer_pos[1]-3]);
        translate([0,screw_sep,0]) cylinder(r=w/2,h=2);
    }
    
    repeat([0,screw_sep,0],2) translate([0,0,2]){
        cylinder(d=screw_d*2, h=999, $fn=16);
        cylinder(d=screw_d, h=999, center=true, $fn=16);
    }
}
    
/*difference(){
    w=stop_screw_sep+screw_d*2+1;
    b=screw_d*2+1;
    h = micrometer_pos[1] + 5;
    hull(){
        translate([-w/2, -b/2, 0]) cube([w,b,h]);
        translate([-w/2, 0, 0]) cube([w,h*2/3,1]);
    }
    repeat([stop_screw_sep,0,0], 2, center=true) translate([0,0,2]){
        cylinder(d=screw_d*2, h=999, $fn=16);
        cylinder(d=screw_d, h=999, center=true, $fn=16);
    }
}*/
