use <../utilities.scad>;

post_r=25/2+0.3;
clamp_h=10;
clamp_t=4;
flat_t=8;
flat_width=15;
bolt_flat_w=clamp_h;
gap_w=3;
$fn=16;

difference(){
	hull(){
		cylinder(h=clamp_h,r=post_r+clamp_t,center=true,$fn=48);
		translate([0,-post_r-flat_t/2,0]) cube([flat_width,flat_t,clamp_h],center=true);
		translate([0,post_r+bolt_flat_w/2,0]) cube([12,bolt_flat_w,clamp_h],center=true);
	}

	cylinder(h=9999,r=post_r,center=true,$fn=48); //post
	translate([0,999,0]) cube([gap_w,999*2,999],center=true); //gap

	//clamping bolt
	translate([gap_w/2+2,post_r+bolt_flat_w/2,0]) rotate([90,0,90]) nut(4,h=999,fudge=1.22,shaft=true);
	translate([-gap_w/2-2,post_r+bolt_flat_w/2,0]) rotate([0,-90,0]) cylinder(r=4.5,h=999);

	//mounting bolt
	translate([0,-post_r-4.5,0]) rotate([-90,0,0]){
		cylinder(r=4.5,h=post_r);
		cylinder(r=2*1.22,h=flat_t*2,center=true);
	}
}