use <utilities.scad>;

/*************************************************************
Simple butt-coupled LED-to-SMA mount for aligning microscopes
with spectrometers coupled by a fibre patch cable.

(c) Richard Bowman 2015, please share under CC-BY 3.0
You can use it for what you like, but attribution would be 
very nice.  Thanks!
*************************************************************/

SMA_r = 3.2/2+0.2; //dimensions of SMA ferrule
SMA_h = 10;
SMA_thread = 4;
spacing = 0.25; //standoff between LED and fibre tip
LED_r = 3/2+0.2; //radius of LED
LED_flange_to_tip = 4.5; //measured 4.35mm from top of LED to top of flange
LED_flange_r = 4/2; //radius of LED flange
LED_flange_h = 1; 
LED_below_flange = 3; //clearance for leads, etc. under LED

LED_h = LED_below_flange+LED_flange_h+LED_flange_to_tip;
h = LED_h + spacing + SMA_h;

$fn=32;
d=0.05;

difference(){
    union(){
        cylinder(r=6,h=h-SMA_thread); //main body
        hull() reflect([1,0,0]) translate([12.5,0,0]) cylinder(r=8,h=9);
        cylinder(r=SMA_r+0.5,h=h);
    }
    
    cylinder(r=LED_flange_r,h=2*LED_below_flange+2*d,center=true);
    translate([0,0,LED_below_flange]) cylinder(r=LED_flange_r,h=LED_flange_h);
    translate([0,0,LED_below_flange+LED_flange_h-d]) cylinder(r1=LED_flange_r,r2=LED_r,h=LED_flange_r-LED_r);
    translate([0,0,LED_below_flange+LED_flange_h]) cylinder(r=LED_r,h=LED_h+d);
    translate([0,0,LED_h]) cylinder(r1=LED_r-0.3,r2=SMA_r-0.3,h=spacing+d);
    translate([0,0,LED_h+spacing]) cylinder(r=SMA_r,h=999);
    
    //mounting screws
    reflect([1,0,0]) translate([12.5,0,3]){
        cylinder(r=6*1.1/2,h=999,center=true);
        cylinder(r=6,h=999);
    }
    
    //access for LED legs (good luck getting them in...
    translate([0,0,LED_below_flange/2]) rotate([30,0,0]) translate([0,999/2,0]) cube([LED_flange_r*2,999,LED_below_flange/2],center=true);
    translate([-LED_flange_r,0,-d]) cube([LED_flange_r*2,4,LED_below_flange*0.75]);
}
