use <../utilities.scad>;
//use <bx51_dovetail.scad>;
use <../standard_optomechanics/cylinder_clamp.scad>;

clamp_h=15;
sm1_radius=15.25+0.35; //rwb27 added the +0.35 to make it fit better
beam_h=50;
camera_h=25;
mounting_hole_x = 8.25;


$fn=48;
d=0.05;

module sm1_clamp(){
    translate([0,0,clamp_h/2]) cylinder_clamp(inner_r=sm1_radius, clamp_h=clamp_h, clamp_t=2, mounting_bolt=0, flat_width=0, flat_t=3);
}

difference(){
	union(){
        sm1_clamp();
        cylinder(r=23/2,h=camera_h); //beam tube to keep stray light out
        
        difference(){
            hull(){
                translate([0,0,clamp_h-d]) resize([0,0,d]) sm1_clamp();
                translate([0,-3.5,camera_h]) cylinder(r=12.5,h=2);
                translate([0,camera_h-clamp_h+sm1_radius,camera_h]) cube([25,d,4],center=true);
            }
            
            //Space to allow for flex
            sequential_hull(){
                translate([0,0,clamp_h-2*d]) cylinder(r=sm1_radius+1, h=d);
                translate([0,0,camera_h-5]) cylinder(r=12.5,h=2);
            }
            
        }
        //clip for end of PCB
        reflect([1,0,0]) translate([-21.5/2-d,camera_h-clamp_h+sm1_radius -3,camera_h])hull(){
            translate([0,0,0.75]) cube([d,3,d]);
            translate([0,0,1.75]) cube([1.1,3,0.25]);
        }
    }
            
    //beam clearance
    sequential_hull(){
        translate([0,0,-d]) cylinder(r=23/2-2, h=d);
        translate([0,0,camera_h-1]) cube([8,8,d],center=true);
        translate([0,0,camera_h]) cube([8,8,10],center=true);
    }

    //mounting holes
    reflect([1,0,0]) translate([mounting_hole_x,0,camera_h-5]) cylinder(r=0.6,h=999,$fn=12); 
    
    //clearance for PCB
    translate([0,0,0]){
        hull(){
            translate([-10/2,-13.5,camera_h]) cube([10,d,8]);
            translate([-21.5/2,-4,camera_h]) cube([21.5,49.5,8]);
        }
        reflect([0,1,0]) hull(){
            translate([-4.5,6,camera_h-1.5]) cube([9,7.5,8]);
            translate([-5.5,6,camera_h-1.5]) cube([11,6.5,8]);
        }
        hull(){
            translate([0,22.5,camera_h+4]) cube([20.5,28,15],center=true);
            translate([0,25.5,clamp_h]) cube([3,28,d],center=true);
        }
    }
    
    //slit for clamp
    translate([-1.5,23/2+d,0]) cube([3,999,999]);
}
 
