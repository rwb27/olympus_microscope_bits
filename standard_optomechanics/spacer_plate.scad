use <utilities.scad>;

module hole(){
    cylinder(r=3*1.15,h=999,center=true,$fn=16);
}

difference(){
    cube([75,50,3],center=true);
    
    reflect([1,0,0]) hull() reflect([0,1,0]) translate([25,15,0]) hole();
    repeat([0,12.5,0],3,center=true) hull() reflect([1,0,0]) translate([12.5,0,0]) hole();
}