use <../utilities.scad>;

//for 1 inch/25mm post clamp, an inner radius of 12.8 is good.
	
module cylinder_clamp(
	inner_r=12.8, //radius of the cylinder being clamped (ish!)
	clamp_h=10, //thickness of the clamp along the cylinder axis
	clamp_t=4, //thickness of the clamp radially
	flat_t=8, //how thick the bottom of the clamp is (often need to allow for the mounting bolt head here)
	flat_width=15 //width of the flat base
	){
	assign(
		post_r = inner_r, //poor initial choice of name!
		bolt_flat_w=10, //width of the region where the bolt is
		gap_w=3 //size of the gap that is squeezed by the bolt
	)
	difference(){
		hull(){
			cylinder(h=clamp_h,r=post_r+clamp_t,center=true,$fn=48);
			translate([0,-post_r-flat_t/2,0]) cube([flat_width,flat_t,clamp_h],center=true);
			translate([0,post_r+bolt_flat_w/2,0]) cube([12,bolt_flat_w,clamp_h],center=true);
		}
	
		cylinder(h=9999,r=post_r,center=true,$fn=48); //post
		translate([0,999,0]) cube([gap_w,999*2,999],center=true); //gap
	
		//clamping bolt
		translate([gap_w/2+2,post_r+bolt_flat_w/2,0]) rotate([90,0,90]) nut(4,h=999,fudge=1.22,shaft=true);
		translate([-gap_w/2-2,post_r+bolt_flat_w/2,0]) rotate([0,-90,0]) cylinder(r=4.5,h=999,$fn=16);
	
		//mounting bolt
		translate([0,-post_r-4.5,0]) rotate([-90,0,0]){
			cylinder(r=4.5,h=post_r,$fn=16);
			cylinder(r=2*1.22,h=flat_t*2,center=true,$fn=16);
		}
	}
}

cylinder_clamp(20, 15);