$fn=16;

difference(){
	union(){
		translate([0,13.5,0])cube([25,67,1],center=true);
		//locating ridge
		translate([0,29-13,0.5]) difference(){
			cube([21,58,1.8],center=true);
			cube([19.5,56.5,999],center=true);
			//cable clearance
			translate([-12,36-29+13,0]) cube([8,8,8],center=true);
		}

		//clip for side
		translate([0,47+1,4]) cube([10,2,9],center=true);
		translate([0,47,6.5]) rotate([0,90,0]) cylinder(r=0.9,h=3.5,center=true);
	}	

	//screw holes for mounting
	for(p=[-1,1]) translate([6*p,-16.5,0]) cylinder(r=1.5*1.15,h=999,center=true);

	//holes for cage rods, 6mm diameter on 30mm spacing
	//(1.01 correction factor to make them print right)
 	for(x=[-1,1]) for(y=[-1,1])
		translate([x,y,0]*15*1.01) cylinder(h=999,r=3.3,center=true);

}