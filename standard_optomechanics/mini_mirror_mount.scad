use <../utilities.scad>;

xyflex_l = 2;
xyflex_t = 1;
zflex_l = 1.5;
zflex_t = 0.75;
bolt_clearance = 4; //amount of clearance to leave around bolt hole
extra_lever = 4; //length of lever after bolt clearance (for elastic band)
strut_t = 3;			//thickness of a "non bendy" part
mirror_size = [12,12]; //size of the mirror
lever_length = [26,26]; //effective length of the levers for X and Z (best keep the same)

//calculated stuff
z_linkage_x = mirror_size[1] * (lever_length[0]-2*bolt_clearance)/lever_length[1];
block_width = lever_length[0]+bolt_clearance+extra_lever;
d=0.05;
$fn=24;

module bolt(){
	cylinder_with_45deg_top(r=3/2,h=999,center=true);
	mirror([0,1,0]) cylinder_with_45deg_top(r=bolt_clearance,h=999);
}

module mini_mirror_mount(){
	difference(){
		union(){
			translate([0,-strut_t,0]) cube([mirror_size[0], strut_t, mirror_size[1]]); //mirror goes here
			translate([0,-strut_t-zflex_l-d,0]) cube([mirror_size[0],zflex_l+2*d,zflex_t]); //z flexure
			difference(){
				translate([0,-2*strut_t-zflex_l,0]) cube([block_width,strut_t,mirror_size[1]]); //x lever
				translate([z_linkage_x,0,mirror_size[1]]) cube([xyflex_t+2,999,zflex_t*2+2],center=true); //clearance for Z linkage
			}
			translate([-xyflex_l-d,-strut_t-zflex_l-xyflex_t,0]) cube([xyflex_l+2*d,xyflex_t,mirror_size[1]]); //pivot for X lever
	
			difference(){
				translate([0,-3*strut_t-zflex_l-xyflex_l,0]) cube([block_width,strut_t,mirror_size[1]]); //z lever
				translate([lever_length[0]-bolt_clearance*2,-99,mirror_size[1]/2]) bolt();
			}
			translate([0,-2*strut_t-zflex_l-xyflex_l-d,0]) cube([xyflex_t,xyflex_l+2*d,mirror_size[1]]); //pivot for Z lever
			translate([z_linkage_x-xyflex_t/2,-2*strut_t-zflex_l-xyflex_l-d,mirror_size[1]-zflex_t]) cube([xyflex_t,strut_t+zflex_l+xyflex_l+2*d,zflex_t]); //clearance for Z linkage
			
		}
		translate([lever_length[0],-2*strut_t-xyflex_l-zflex_l+d,mirror_size[1]/2]) bolt();
	}
}

union(){
	translate([-4-xyflex_l,-2*strut_t,0]) cube([4,2*strut_t+10,mirror_size[1]]);
	difference(){
		translate([-d-xyflex_l,6,0]) cube([block_width+2+2*d+xyflex_l,4,mirror_size[1]]);
		translate([mirror_size[0]/2,0,mirror_size[1]/2]) cylinder_with_45deg_top(r=4,h=999);
	}
	translate([block_width+2,-d,0]) cube([4,10+d,mirror_size[1]]);
	translate([mirror_size[0]+6,-strut_t+1,0]) cube([block_width-mirror_size[0],strut_t+0.5,mirror_size[1]]);
	translate([-xyflex_l-d,1,0]) cube([xyflex_l+2*d+block_width+2,7,0.7]);

	mini_mirror_mount();
}