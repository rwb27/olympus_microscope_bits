use <./cageplate2.scad>;
use <../piscope_5/working_set/picam_push_fit.scad>;

difference(){
    translate([0,0,5]) cageplate();
    picam_push_fit_2();
}