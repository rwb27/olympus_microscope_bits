use <../utilities.scad>;
use <cylinder_clamp.scad>;

big_r=25/2+0.15; //first try was 13.3 which came out to 26.3mm diameter
small_r=25/4+0.1; //first try was 6.65 which came out to 13mm diameter and even metric posts seem to be 12.65 rather than 12.5mm diameter (umm, think I had old posts, oops!)
separation=25;

union(){
	difference(){
		rotate([0,0,90]) cylinder_clamp(big_r,small_r*2+3*2,flat_t=separation-big_r-small_r-0.5,flat_width=small_r*2,mounting_bolt=0);
		translate([0,0,small_r+4/2*1.2]) rotate([0,0,-90]) nut_y(4,h=separation-small_r+0.5);
	}
	translate([separation,0,0]) rotate([0,0,180]) cylinder_clamp_v(small_r,small_r*2,flat_t=3,mounting_bolt=0,flat_w=2*small_r,clamp_t=4);
}