use <utilities.scad>;
$fn=16;

module clamp_screw_hole(){
	translate([0,0,-1]) union(){
		cylinder(r=1.45, h=10);
		cylinder(r=1.8, h=5);
		cylinder(r=3, h=3);
	}
}

module cageplate(){
	difference(){
		minkowski(){ //rounded rectangle, cageplate-sized
			cube([36,36,9],center=true);
			cylinder(h=1,r=2,center=true);
		}
		//holes for cage rods, 6mm diameter on 30mm spacing
		//(1.01 correction factor to make them print right)
 		for(x=[-1,1]) for(y=[-1,1])
			translate([x,y,0]*15*1.01) cylinder(h=999,r=3.3,center=true);
		//side cut-outs and clamp screw holes
		for(p=[1,-1]){
			translate([-16*p,0,0]) cube([2,30,999],center=true);
			for(q=[-1,1])
				translate([-20*p,-9*q,0]) rotate([0,p*90,0]) clamp_screw_hole();
		}
	}
}

intersection(){
	difference(){
		union(){
			translate([0,0,1]) cageplate();
			translate([0,33,1]) cube([25,28,10],center=true);
		}
		//viewport hole
		cylinder(r1=8,r2=1,h=12,center=true);
	
		//clearance for PCB
		translate([0,29-13,5]) cube([21.5,58.5,8],center=true);
		translate([0,0,5]) cube([12,26,12],center=true);
		translate([0,22.5,5]) cube([21,33,15],center=true);
		translate([-3,41,5]) cube([15,6,15],center=true);
	
		//mounting holes
		for(p=[[-8.25,0,0],[8.25,0,0],[-4.5,-9,0],[6.7,42,0]]) 
			translate(p+[0,0,-2]) cylinder(r=0.5,h=999);
	
		//screw holes for mounting back
		for(p=[-1,1]) translate([6*p,-16.5,0]) cylinder(r=1.5,h=9);
		translate([0,47,0]) cube([4,2,2],center=true);
	
		//cable clearance
		translate([0,36,4.5]) rotate([0,90,0]) cylinder(h=999, r=1.8);
		translate([0,36-1.8,4.5]) cube([999,3.6,999]);
	}
	union(){
		cube([24,999,999],center=true);
		translate([0,15+5,0]) cube([999,10,999],center=true);
		reflect([1,0,0]) translate([15+5,15,0]) cube([10,10,999],center=true);
	}
}	