use <../utilities.scad>;
//use <bx51_dovetail.scad>;
use <../standard_optomechanics/cylinder_clamp.scad>;

clamp_h=15;
sm1_radius=15.25;
beam_h=50;


$fn=48;
d=0.05;

difference(){
	union(){
		cylinder_clamp(inner_r=sm1_radius, clamp_h=clamp_h, mounting_bolt=0, flat_width=15, flat_t=beam_h-sm1_radius);
		hull() reflect([1,0,0]) translate([12.5,-beam_h,0]) rotate([-90,0,0]) cylinder(r=clamp_h/2, h=4, $fn=16);
	}
	reflect([1,0,0]) translate([12.5,-beam_h,0]) rotate([-90,0,0]){
		cylinder(r=6/2*1.1, h=20, $fn=16, center=true);
		translate([0,0,4]) cylinder(r=10/2*1.1, h=20, $fn=16);
	}
}